# Change log

This document summerizes the cumulative changes in genome annoations
since the initial annotation retieved from the EBI.

## v1.2 (2021-05-18)

### Edited

* Delete all standard_name qualifiers.

## v1.1 (2021-04-28)

### Edited

* Build the locus hierarchy

### Fixed

* Bad mRNA coordinates in locus LADA_0G04654G.
* Bad CDS coordinates in locus LADA_0C13102G.

## v1.0 (2021-04-28)

### Added

* The 8 annotated chromosomes of Lachancea dasiensis CBS 10888 (source EBI, [GCA_900074725.1](https://www.ebi.ac.uk/ena/browser/view/GCA_900074725.1)).
