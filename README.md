## *Lachancea dasiensis* CBS 10888

### Source

* **Data source**: [ENA](https://www.ebi.ac.uk/ena/browser/home)
* **BioProject**: [PRJEB12928](https://www.ebi.ac.uk/ena/browser/view/PRJEB12928)
* **Assembly accession**: [GCA_900074725.1](https://www.ebi.ac.uk/ena/browser/view/GCA_900074725.1)
* **Original submitter**: 

### Assembly overview

* **Assembly level**: Chromosome
* **Assembly name**: LADA0
* **Assembly length**: 10,701,617
* **#Chromosomes**: 8
* **Mitochondiral**: No
* **N50 (L50)**: 1,410,526 (4)

### Annotation overview

* **Original annotator**: 
* **CDS count**: 5162
* **Pseudogene count**: 66
* **tRNA count**: 204
* **rRNA count**: 9
* **Mobile element count**: 10
